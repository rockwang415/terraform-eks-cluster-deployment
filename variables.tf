variable "cluster-name" {
  default = "A-Lovely-Kubernetes-Cluster"
  type    = string
}

variable "aws_region" {
  default = "ap-southeast-2"
  type    = string
}

variable "k8s_bastion_node_instance_type" {
  default = "t2.nano"
  type    = string
}

variable "k8s_bastion_node_ami" {
  default = "ami-080660c9757080771"
  type    = string
}

variable "k8s_bastion_node_key_name" {
  default = "rockaws"
  type    = string
}

variable "worker_nodes_desired_size" {
  default = "1"
  type    = string
}

variable "worker_nodes_max_size" {
  default = "1"
  type    = string
}

variable "worker_nodes_min_size" {
  default = "1"
  type    = string
}
